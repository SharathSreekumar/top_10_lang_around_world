# Top 10 Languages Around the World

The project is divided into 3 sections:

* Front-End
* Back-End
* Database

## Front-End
This project uses React + Redux (written in TypeScript) as UI (User Interface).

## Back-End
This project uses Node + Express (written in TypeScript) for the BE (BackEnd).

## Database
The database used for this project is **CouchDB**.

To use the docker version you will need to download the image.
> docker pull couchdb

For further details, refer https://hub.docker.com/_/couchdb.

To Test if CouchDB is up, try to access CouchDB from Terminal request:
> curl -X PUT http://<COUCH_DB_USERNAME>:<COUCH_DB_PASSWORD>@127.0.0.1:5984/test

The response should be:
> {"ok": true}

# NOTE:
This project is still on **Work In Progress**.