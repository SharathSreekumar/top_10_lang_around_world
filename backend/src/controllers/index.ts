import { NextFunction, Request, Response } from 'express';
// import * as nano from 'nano';
import * as request from 'request';
/* import {
    getApiUrl,
    urlParams
} from '../utilities'; */

const couchDBURL = `http://${process.env.COUCHDB_USER}:${
  process.env.COUCHDB_PASSWORD
}@${process.env.COUCH_DB_HOST}:${process.env.COUCH_DB_PORT}`;
// const couchDB = nano(couchDB_URL);

function statusHandler(req: Request, res: Response, next: NextFunction): void {
  res.status(200).json({ message: 'Status Ok!', env: process.env });
}

function dbStatusHandler(req: Request, res: Response, next: NextFunction): any {
  try {
    // const dbList: any = await couchDB.db.list();
    // res.status(200).json({ status: dbList && dbList.length > 0 ? 'OK' : 'NOK', db_list: dbList });
    console.log('DB_URL', `${couchDBURL}/test`);

    // const dbStatus = request.put(`${couchDB_URL}/test`);
    request(
      //   { method: 'PUT', url: `${couchDBURL}/test` },
      { method: 'GET', url: `${couchDBURL}` },
      (error: any, response: any, body: any) => {
        if (body) {
          res.status(200).json({ body });
        } else if (response) {
          res.status(200).json({ response });
        } else if (error) {
          res.status(400).json({ error_message: 'DB error', error });
        } else {
          res.status(400).json({ body: 'No response from DB' });
        }
      }
    );
  } catch (error) {
    res.status(500).json({ error });
  }
}

export { statusHandler, dbStatusHandler };
